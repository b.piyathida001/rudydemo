import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rudy/models/customer_model.dart';
import 'package:rudy/service/auth_service.dart';
import 'package:rudy/widget/customer_form.dart';

class CustomerPopup extends StatelessWidget {
  final CustomerModel customer;
  final AuthService auth;

  CustomerPopup({this.customer,this.auth});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30)),
          color: Colors.white),
      height: MediaQuery.of(context).size.height * 0.5,
      child: CustomerForm(
        customer: customer,
        auth: auth,
      ),
    );
  }
}
