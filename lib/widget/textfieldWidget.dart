import 'package:flutter/material.dart';

class TextFieldWidget {
  static textField(
      {@required TextEditingController controller,
      @required String label,
      bool obscureText = false,
      Function func}) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
      child: TextFormField(
        onChanged: (value) {
          if (func != null) {
            func();
          }
        },
        controller: controller,
        decoration:
            InputDecoration(border: OutlineInputBorder(), labelText: label),
        obscureText: obscureText,
        style: TextStyle(fontSize: 18),
      ),
    );
  }
}
