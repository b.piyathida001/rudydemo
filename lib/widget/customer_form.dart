import 'dart:developer';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rudy/models/customer_model.dart';
import 'package:rudy/service/auth_service.dart';
import 'package:rudy/service/database.dart';
import 'package:rudy/widget/textfieldWidget.dart';

class CustomerForm extends StatefulWidget {
  final CustomerModel customer;
  final AuthService auth;

  CustomerForm({this.customer, this.auth});

  @override
  _CustomerFormState createState() => _CustomerFormState();
}

class _CustomerFormState extends State<CustomerForm> {
  Database db = Database.instance;
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.customer != null) {
      _firstNameController.text = widget.customer.firstName;
      _lastNameController.text = widget.customer.lastName;
      _phoneController.text =
          widget.customer.phone == '-' ? "" : widget.customer.phone;
    }
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _phoneController.dispose();
    super.dispose();
  }

  Future<void> _confirmData() async {
    if (_firstNameController.text == '' || _lastNameController.text == '') {
      print('กรุณากรอกข้อมูลให้ครบถ้วน');
    } else {
      String newCustomerId = 'CID${DateTime.now().millisecondsSinceEpoch.toString()}';
      await db.setCustomer(
          customer: CustomerModel(
              uid: widget.auth.currentUser.uid,
              id: widget.customer != null ? widget.customer.id : newCustomerId,
              firstName: _firstNameController.text,
              lastName: _lastNameController.text,
              phone: _phoneController.text));
      Navigator.of(context).pop();
    }
  }

  void _cancelData() {
    Navigator.of(context).pop();
  }

  Widget _showOKButton() {
    return RaisedButton(
      color: Colors.amber,
      onPressed: _confirmData,
      child: Text(
        widget.customer != null ? 'แก้ไข' : 'เพิ่ม',
        style: buttonStyle(),
      ),
    );
  }

  Widget _showCancelButton() {
    return OutlinedButton(
      onPressed: _cancelData,
      child: Text(
        'ปิด',
        style: buttonStyle(color: Colors.grey[700]),
      ),
    );
  }

  TextStyle buttonStyle({Color color = Colors.white}) {
    return TextStyle(color: color, fontSize: 18);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(widget.customer != null ? 'แก้ไข' : 'เพิ่ม',style: TextStyle(fontSize: 35,color: Colors.grey[700]),),
          TextFieldWidget.textField(
              controller: _firstNameController, label: "ชื่อ"),
          TextFieldWidget.textField(
              controller: _lastNameController, label: "นามสกุล"),
          TextFieldWidget.textField(
              controller: _phoneController, label: "เบอร์โทรศัพท์"),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _showOKButton(),
              SizedBox(
                width: 20,
              ),
              _showCancelButton()
            ],
          )
        ],
      ),
    );
  }
}
