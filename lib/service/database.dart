import 'dart:developer';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:rudy/models/customer_model.dart';

class Database {
  static Database instance = Database._();

  Database._();

  Future<String> getDataFromPath({@required String path}) async {
    final reference = FirebaseFirestore.instance.doc(path);
    try {
      final docSnapshot = await reference.get();
      if (docSnapshot.exists) {
        return docSnapshot.data().toString();
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  Stream<List<CustomerModel>> getCustomers({String uid}) {
    final reference = FirebaseFirestore.instance.collection('/customers');
    try {
      // Query query = reference.orderBy("id",descending: true);
      Query query = reference.where('uid', isEqualTo: uid);
        final snapshots = query.snapshots();
        return snapshots.map((snapshots) {
          return snapshots.docs.map((doc) {
            return CustomerModel.fromMap(doc.data());
          }).toList();
      });
    } catch (err) {
      print(err);
    }
  }

  Future<void> setCustomer({CustomerModel customer}) async {
    final reference =
        FirebaseFirestore.instance.doc('/customers/${customer.id}');
    try {
      await reference.set(customer.toMap());
    } catch (err) {
      rethrow;
    }
  }

  Future<void> deleteCustomer({CustomerModel customer}) async {
    final reference =
        FirebaseFirestore.instance.doc('/customers/${customer.id}');
    try {
      await reference.delete();
    } catch (err) {
      rethrow;
    }
  }
}
