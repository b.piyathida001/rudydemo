import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AuthService {

  FirebaseAuth auth = FirebaseAuth.instance;
  User get currentUser => auth.currentUser;

  Stream<User> authStateChanges(){
    return auth.authStateChanges();
  }

  Future<void> login({@required String email,@required String password}) async {
    try {
      await auth.signInWithEmailAndPassword(email: email, password: password);
    } catch (err) {
      throw FirebaseAuthException(message: err.toString());
    }
  }

  Future<void> logout() async {
    try {
      await auth.signOut();
    } catch (err) {
      rethrow;
    }
  }
}