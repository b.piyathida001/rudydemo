import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:rudy/models/customer_model.dart';
import 'package:rudy/service/auth_service.dart';
import 'package:rudy/service/database.dart';
import 'package:rudy/widget/customer_popup.dart';

class CustomersScreen extends StatefulWidget {
  CustomersScreen({Key key, this.auth}) : super(key: key);
  final AuthService auth;

  @override
  _CustomersScreenState createState() => _CustomersScreenState();
}

class _CustomersScreenState extends State<CustomersScreen> {
  Database db = Database.instance;
  final SlidableController slidableController = SlidableController();

  _goToCustomerForm({CustomerModel customer}) {
    log("add customer");
    showModalBottomSheet(
        isScrollControlled: true,
        useRootNavigator: true,
        context: context,
        builder: (context) => CustomerPopup(
              customer: customer,
              auth: widget.auth,
            ));
  }

  TextStyle cellStyle() => TextStyle(fontSize: 18.0, color: Colors.grey[700]);

  Widget dataCell({@required CustomerModel customer}) {
    return Slidable(
      controller: slidableController,
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.15,
      secondaryActions: [
        IconSlideAction(
          iconWidget: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.red[600]),
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.only(left: 5, right: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                // crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.delete_forever,
                    color: Colors.white,
                    size: 28,
                  ),
                  Text(
                    "ลบ",
                    style: TextStyle(color: Colors.white),
                  )
                ],
              )),
          onTap: () {
            db.deleteCustomer(customer: customer);
          },
        )
      ],
      key: UniqueKey(),
      child: GestureDetector(
        onTap: () {
          _goToCustomerForm(customer: customer);
        },
        child: Container(
          padding: EdgeInsets.all(10.0),
          margin: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.amber),
              borderRadius: BorderRadius.circular(5)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "ชื่อ-นามสกุล :  ${customer.firstName} ${customer.lastName}",
                    style: cellStyle(),
                  ),
                  Text(
                    "เบอร์โทรศัพท์ : ${customer.phone}",
                    style: cellStyle(),
                  ),
                ],
              ),
              Icon(
                Icons.arrow_forward_ios_outlined,
                color: Colors.amber,
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        leadingWidth: 15,
        iconTheme: IconThemeData(color: Colors.white),
        title: Row(
          children: [
            Text(
              "Customers",
              style: TextStyle(fontSize: 28, color: Colors.white),
            ),
          ],
        ),
      ),
      body: StreamBuilder<List<CustomerModel>>(
        stream: db.getCustomers(uid: widget.auth.currentUser.uid),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.length == 0) {
              return Center(
                child: Text("ยังไม่มีรายชื่อลูกค้า"),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return dataCell(customer: snapshot.data[index]);
                  });
            }
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _goToCustomerForm,
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 40,
        ),
      ),
    );
  }
}
