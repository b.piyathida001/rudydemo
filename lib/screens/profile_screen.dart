import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rudy/service/auth_service.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key key, this.auth}) : super(key: key);
  final AuthService auth;

  TextStyle pStyle = TextStyle(fontSize: 20.0, color: Colors.grey[700]);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          leadingWidth: 15,
          iconTheme: IconThemeData(color: Colors.white),
          title: Row(

            children: [
              Text(
                "Profile",
                style: TextStyle(fontSize: 28, color: Colors.white),
              ),
            ],
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "email: " + auth.currentUser.email,
                style: pStyle,
              ),SizedBox(height: 20,),
              Text(
                "UID: " + auth.currentUser.uid,
                style: pStyle,
              )
            ],
          ),
        ));
  }
}
