import 'dart:async';
import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:rudy/screens/loading_screen.dart';
import 'package:rudy/service/auth_service.dart';
import 'package:rudy/widget/textfieldWidget.dart';

import 'home_screen.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _username;
  TextEditingController _password;
  bool _isLoading = false;
  bool _isErrText = false;
  AuthService auth;
  Stream<User> user;
  String errText = '';

  Future<void> loginAction() async {
    if (_username.text == '' || _password.text == '') {
      log("err");
      setState(() {
        _isErrText = true;
        errText = 'Please complete the information.';
      });
    } else {
      setState(() {
        _isLoading = true;
      });
      try {
        await auth
            .login(email: _username.text, password: _password.text)
            .then((value) {
          setState(() {
            _isLoading = false;
            _isErrText = false;
          });
          log("send home page");
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomeScreen(
                      auth: auth,
                    )),
          );
        });
      } catch (err) {
        log(err.toString());
        errText = getErrorMessage(err.toString());
      }
      setState(() {
        _isLoading = false;
        _isErrText = true;
      });
    }
  }

  String getErrorMessage(String errText) {
    List<String> result = errText.split("]");
    return result.last;
  }

  void clearErr() {
    if (_isErrText) {
      setState(() {
        _isErrText = false;
        errText = '';
      });
    }
  }

  Widget _buildErrorText() {
    return Visibility(
        visible: _isErrText,
        child: Padding(
          padding: EdgeInsets.only(left: 20, top: 20, right: 20),
          child: Text(
            errText,
            softWrap: true,
            style: TextStyle(fontSize: 14, color: Colors.red),
          ),
        ));
  }

  Widget loginButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 8),
      child: InkWell(
        onTap: loginAction,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.amber, borderRadius: BorderRadius.circular(5)),
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Login",
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    auth = AuthService();
    user = auth.authStateChanges();
    _username = TextEditingController();
    _password = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    _username.dispose();
    _password.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      'Login',
                      style: TextStyle(fontSize: 35.0, color: Colors.grey[700]),
                    ),
                  ),
                ],
              ),
              TextFieldWidget.textField(
                  controller: _username, label: "Username", func: clearErr),
              TextFieldWidget.textField(
                  controller: _password,
                  label: "Password",
                  obscureText: true,
                  func: clearErr),
              _buildErrorText(),
              loginButton(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                      onPressed: () {},
                      child: Text(
                        "Forgot password",
                        style: TextStyle(color: Colors.grey[700]),
                      )),
                ],
              )
            ],
          ),
        ),
        _isLoading ? LoadingScreen() : Container()
      ],
    ));
  }
}
