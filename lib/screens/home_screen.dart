import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:rudy/screens/login_screen.dart';
import 'package:rudy/service/auth_service.dart';

import 'customers_screen.dart';
import 'profile_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({
    Key key,
    this.auth,
  }) : super(key: key);
  final AuthService auth;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  void goToCustomersScreen() {
    log("send customers page");
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => CustomersScreen(
                auth: widget.auth,
              )),
    );
  }

  void goToProfileScreen() {
    log("send profile page");
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ProfileScreen(
                auth: widget.auth,
              )),
    );
  }

  Future<void> logOut() async {
    log("log out");
    await widget.auth.logout();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginScreen()),
    );
  }

  Widget card(
      {@required String title,
      @required Function func,
      IconData icon = Icons.circle}) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: InkWell(
        onTap: func,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5), // if you need this
            side: BorderSide(
              color: Colors.amber.withOpacity(0.8),
              width: 3,
            ),
          ),
          child: Container(
            margin: EdgeInsets.fromLTRB(10, 20, 10, 20),
            width: 100,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  icon,
                  color: Colors.amber,
                  size: 50,
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  title,
                  style: TextStyle(fontSize: 18, color: Colors.grey[700]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Container(
            color: Colors.amber,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.auth.currentUser.email,
                  style: TextStyle(fontSize: 28, color: Colors.white),
                ),
                InkWell(
                  onTap: () {
                    logOut();
                  },
                  child: Icon(
                    Icons.logout,
                    color: Colors.white,
                    size: 30,
                  ),
                )
              ],
            ),
          ),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            card(title: "Customers", func: goToCustomersScreen,icon: Icons.book_outlined),
            card(title: "Profile", func: goToProfileScreen,icon: Icons.perm_identity_outlined)
          ],
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
