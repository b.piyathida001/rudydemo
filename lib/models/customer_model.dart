import 'package:flutter/material.dart';

class CustomerModel {
  String uid;
  String id;
  String firstName;
  String lastName;
  String phone;

  CustomerModel({
    @required this.uid,
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    this.phone,
  });

  factory CustomerModel.fromMap(Map< String, dynamic> customer) {
    if (customer == null) {
      return null;
    }
    String uid = customer['uid'];
    String id = customer['id'];
    String firstName = customer['firstName'];
    String lastName = customer['lastName'];
    String phone = customer['phone'] == '' ? '-' : customer['phone'];

    return CustomerModel(uid: uid,id: id, firstName: firstName, lastName: lastName,phone: phone);
  }

  Map<String, dynamic> toMap() {
    return {
      'uid' : uid,
      'id' : id,
      'firstName' : firstName,
      'lastName' : lastName,
      'phone' : phone,
    };
  }


}
